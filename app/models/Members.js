var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

var MySchema = new Schema({
  order: Number,
  name: String,
  birthplace: String,
  birthday: String,
  gender: String,
  education: String,
  phone: String,
  email: String,
  city: String,
  state: String,
  affiliate: String,
  title: String,
  transfer: String
});

module.exports = mongoose.model("Member", MySchema);
