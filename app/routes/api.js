var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Members = require("../models/Members.js");
let multer = require("multer");
let storage = multer.memoryStorage();
let upload = multer({ storage: storage });
let axios = require("axios");
var cors = require("cors");
let credentials = require("../config.js");

router.get("/test", async function(req, res) {
  console.log("hello");
  res.send("ok");
});

router.get("/all", async function(req, res) {
  let members = await Members.find({});
  res.json({ members });
});

router.post("/check", async function(req, res) {
  let member = await Members.findOne({ _id: req.body._id });
  res.json({ member });
});

router.get("/order", async function(req, res) {
  let members = await Members.find({});
  res.json({ order: members.length });
});

router.post("/submit", async function(req, res) {
  console.log(req.body);
  let members = new Members({
    ...req.body
  });
  await members.save();
  res.json({
    id: members._id
  });
});

router.post("/upload", upload.single("avatar"), cors(), function(req, res) {
  let image = req.file.buffer.toString("base64");
  axios({
    method: "post",
    url: "https://api.imgur.com/3/image",
    data: {
      image: image,
      album: "y6Y7amj",
      type: "base64"
    },
    headers: {
      Authorization: credentials.imgur.clientId,
      Authorization: credentials.imgur.brearer
    }
  })
    .then(packet => {
      let url = packet.data.data.link;
      console.log(url);
      res.send({ link: url });
    })
    .catch(error => console.log(error));
});

router.get("/all", async function(req, res) {
  Model.find({}, function(err, docs) {
    let package = docs.map(x => {
      return {
        profile: x.profile,
        stats: x.stats
      };
    });
    res.json(package);
  });
});

module.exports = router;
