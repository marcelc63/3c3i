//Dependencies
const express = require("express");
const app = express();
let http = require("http").Server(app);
let port = 3008;
var path = require("path");
const history = require("connect-history-api-fallback");

var bodyParser = require("body-parser");

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  res.header("Access-Control-Allow-Methods", "*");
  next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var mongoose = require("mongoose");
var credentials = require("./config.js");
mongoose.Promise = require("bluebird");
mongoose
  .connect(credentials.mongoConnection, {
    promiseLibrary: require("bluebird"),
    useNewUrlParser: true,
    auth: { authdb: "admin" },
    useFindAndModify: false
  })
  .then(() => console.log("connection succesful"))
  .catch(err => console.error(err));
mongoose.set("useCreateIndex", true);

var api = require("./routes/api");
app.use("/api", api);

//Basic
app.use(history());
app.use(express.static(__dirname + '/../dist/spa'))

app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname + '/../dist/spa/index.html'))
})

//Initiate
http.listen(port, function() {
  console.log("listening on *:", port);
});
