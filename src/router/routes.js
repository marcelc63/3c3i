const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/Index.vue") },
      { path: "register", component: () => import("pages/Register.vue") },
      { path: "reader", component: () => import("pages/Reader.vue") },
      {
        path: "confirmation/:id",
        component: () => import("pages/confirmation.vue")
      },
      { path: "admin", component: () => import("pages/Admin.vue") }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
